# Copyright IRSOLS Inc. 2019
# Version : 0.6
# Last modified : 06/01/2019

# Objective :
  Create a simple container from a docker file
    - Based on ubuntu:latest and then applications installed on top
    - Get introduced to portability

# Pre-Requisites
Have a local docker environment running and/or access to one in cloud. For this lab use the provided login information
to login to your pod and follow steps below.

Steps :
#Create Directory first
cd # change to your homedir
mkdir lab1
touch Dockerfile
