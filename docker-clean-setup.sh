#!/bin/bash
#
# Provided by IRSOLS Inc
# Setup Docker CE instead of vanilla centos repos
# version 0.3
# last modified 06/06/2019

# If running in VmWare VM
# Install VM tools
yum install -y open-vm-tools

#TODO: Check OS version and change installer accordingly

# Remove all docker versions
yum -y remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
# Setup Docker repository
yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

# Setup stable repo
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

# Install the latest version of Docker CE and containerd
yum -y install docker-ce docker-ce-cli containerd.io

# Start and enable Docker Daemon
systemctl start docker
systemctl enable docker

# Install and setup  Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/doc
ker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version


# Install and setup docker machine
base=https://github.com/docker/machine/releases/download/v0.16.0 &&
  curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
  sudo install /tmp/docker-machine /usr/local/bin/docker-machine
docker-machine version


