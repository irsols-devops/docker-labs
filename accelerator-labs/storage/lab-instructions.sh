# Copyright IRSOLS Inc. 2019
# Version : 0.6
# Last modified : 06/01/2019


# Container Storage Lab


#Create a volume:

$ docker volume create vol1
$ docker volume create vol2

#List volumes:

$ docker volume ls

# Inspect a volume:

$ docker volume inspect vol2
[
    {
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/vol2/_data",
        "Name": "vol2",
        "Options": {},
        "Scope": "local"
    }
]

# Observe all the volumes mounted locally on a host
# Sometimes you need to quickly go and check out the volume mounts and directories for all the containers
# on the host system .
# 1 . You can do that using docker vol ls , inspect and then look for MountPoint one by one on each container
# 2.  Or use our quick utility that automates this . Use the check_vol_mounts.sh in the docker-labs/storage directory

Remove a volume:

$ docker volume rm vol1


# Start a container with a Volume . You can do that with either --mount or --volume option directly . Subtle differences but
# important to stick to one . We'll use --volume or -v option

# Syntax is :
#  docker run -d --name <container_name> -v <host_volume>:<container_volume> <Container_to_launch>
# Example
 docker volume ls
 docker run -d --name devtest -v vol3:/app nginx:latest

 # Observe that vol3 didnt exist before and as soon as we specify that it creates a 'vol3' and then launches the container mapped to it

 # Share data from Host to container

 cd /tmp
 mkdir data
 # copy the sample_index.html to /tmp/data/index.html
 cp sample_index.html /tmp/data/index.html

 # By default nginx uses /usr/share/nginx/html/ directory for storing the
 # html files including index. By pre-populating and mapping the directory
 # to nginx container we're overiding the default web page .
 # Any time we modify the file on host , it will reflect on container.
 # Create a container

 docker run -itd -p 8089:80 --name nginx-test -v /tmp/data:/usr/share/nginx/html/ nginx:latest

 # point your browser to machine_IP:8089
 # Edit the /tmp/data/index.html file and remove the xxxx from the file
 # reload the web browser

# Stretch target (Time permitting) or as a home exercise
# Create multiple containers and share data b/w them and host
# Launch three nginx server containers all mapped to /tmp/data as above
# change index file and observe the behavior with web browser

# Discussion
