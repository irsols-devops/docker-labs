

# Managing existing running containers
# Create a docker container
docker run -itd -p 8089:80 --name nginx-base -v /tmp/data:/data nginx:latest

# Check it w/ host machine IP:8089 and you'll seee the NGINX welcome page

#Here you can't do a docker attach nginx-base ( giving 'attach' the name of the container ), since the app container doesnt have a shell yet. So how do you access , config the app and install any additional software on the app container ?

#You'll have to issue 'exec' command and then attach the current terminal to appcontainer's terminal

#        Syntax:  docker exec -it [container_name] [exexuteable_inside_container]

#       Example : Assuming we already ran 'docker run -itd ...'

docker exec -it nginx-base bash

#This will give the shell to app container 'nginx-base' . Also note when you exit the shell from the container terminal , the app container will still be alive/running since it was started w/ a different process id . If you do a docker ps -a , the nginx-base will still be around and show up as 'Up'.

#  To stop this app container you'll have to do following :

        docker stop nginx-base
### Managing Long Running Containers using hostnames (for FQDN, micro websites etc)
#    Most often containers are going to be shortlived so no need to change the hostname . Docker will assign the CONTAINER_ID as the hostname of the new spun up container . If you want to craete a long runnning container then you'll have to use the --hostname option when spinning up the container for the first time

        docker run -itd --name centos_dslv1 --hostname centos_dslv1 -v /tmp/data:/data centos:latest

## 'Saving' Container States:
### Saving container images locally
    Once a container is created you can access it by either the name or the long container id when you do a 'docker ps'

        e.g. docker exec -it CONTAINER_NAME bash    ---or----  docker exec -it CONTAINER_ID bash

#    Once in the container shell you can add , modify packages or change the config of the container. You'll have to exit the container and then do a commit in order to save the modifications locally.

#    Example steps :
            1. docker run -itd -p 8089:80 --name nginx-base -v /tmp/data:/data nginx:latest
            2. docker exec -it nginx-base bash
            (On the container shell)
            3. apt update -y
            4. apt install nano
            5. exit
            (On host terminal)
            Commit the container locally using either:
             docker commit CONTAINER_NAME nginx-template  ---or--- docker commit CONTAINER_ID nginx-template
            7. docker commit nginx-new-base nginx-template


# Creating ‘golden template’ containers and re-using
#---------------------------------------------------
#At this point, you can spin up a new container, using the new image, and have all the modifications already in place.
#If you do a 'docker images' it will show 'nginx-new-base' as one of the images available to us to launch new container instances based off of that

#Remember, when you run the new container, the command would look something like:

    docker run -itd --name nginx-dev -p 9090:80 -e TERM=xterm -d nginx-new-base


# Managing Container Lifecycle
#------------------------------

# Brute Force
#Docker provides a single command that will clean up any resources — images, containers, volumes, and networks — that are dangling (not associated with a container):

 docker system prune

#To additionally remove any stopped containers and all unused images (not just dangling images), add the -a flag to the command:

 docker system prune -a

 Docker image list : docker images -a
 docker images -f dangling=true
 Remove docker image : docker rmi Image Image
 docker images purge

 # Selective removal
 # If you have a bunch of containers to delete in one go, copy-pasting IDs can be tedious. In that case, you can simply run -

docker rm $(docker ps -a -q -f status=exited)


# Stretch Target: (Time permitting)
#For changing the hostname of an already running container :
#Run following to get hostname/ container id
docker inspect -f ‘{{ .State.Pid }}’ <existing_docker_hostname>
#Output will be some number <15580>

#Run this command to login to the container with its id
nsenter --target 15580 --uts

#Run this command to change the hostname hostname "node_js". Now exit the container and login again you will see the hostname has been changed.
#This way you can assign FQDN to the container

